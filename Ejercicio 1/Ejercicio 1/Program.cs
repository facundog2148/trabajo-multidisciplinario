﻿using System;

namespace Ejercicio_1
{
    class Cuenta
    {
        private string titular;
        private double cantidad;

        public Cuenta(string titular)
        {
            this.titular = titular;
        }
        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            if(cantidad < 0)
            {
                this.cantidad = 0;
            } else
            {
                this.cantidad = cantidad;
            }
        }

        public void setTitular (string titular)
        {
            this.titular = titular;
        }
        

        public void setCantidad(double cantidad)
        {
            this.cantidad = cantidad;
        }

       


    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
